## This README gives instructions to use the following functions in MATLAB for calucaltion of disparity maps from stereo images.

----
### varianceMap()

>Inputs: Left Image (var: leftImage), Right Image (var: rightImage), Window Size (var: windowSize), Minimum Disparity (dispMin), Maximum Disparity (dispMax)

>Outputs: Variance Map (var: varianceImg) , Time taken (var: timeTaken)

----
## usage

    [varianceImg, timeTaken] = varianceMap('MonopolyLeftColor.png', 5, 9);





### funcSSDR2L()

>Inputs: Input Image (var: inputImage), Window Size (var: windowSize), Threshold (var: thresh) Typical value is 140

>Outputs: Disparity Map (var: dispMap), Time taken (var: timeTaken)

----
## usage

    [dispMap, timeTaken] = funcSSDR2L('StereogramLeft.jpg', 'StereogramRight.jpg', 9, 0, 16);






### funcSHDR2L()

>Inputs: Input Image (var: inputImage), Window Size (var: windowSize), Threshold (var: thresh) Typical value is 140

>Outputs: Disparity Map (var: dispMap), Time taken (var: timeTaken)

----
## usage

    [dispMap, timeTaken] = funcSHDR2L('StereogramLeft.jpg', 'StereogramRight.jpg', 9, 0, 16);
