% *************************************************************************
% Title: Function-Rank Transform of a given Image
% Inputs: Image (var: inputImage), Window size assuming square window (var:
% windowSize)
% Outputs: Rank Tranformed Image (var: rankTransformedImage), 
% Time taken (var: timeTaken)
% Example Usage of Function: [a,b]=funcRankOneImage('Img.png', 3)
% *************************************************************************
function [rankTransformedImage, timeTaken] = funcRankOneImage(inputImage, windowSize)
% Grab the image information (metadata) using the function imfinfo
imageInfo=imfinfo(inputImage);
% Since Rank Transform is applied on a grayscale image, determine if the
% input image is already in grayscale or color
if(getfield(imageInfo,'ColorType')=='truecolor')
% Read an image using imread function, convert from RGB color space to
% grayscale using rgb2gray function and assign it to variable inputImage
    inputImage=rgb2gray(imread(inputImage));
else if(getfield(imageInfo,'ColorType')=='grayscale')
% If the image is already in grayscale, then just read it.        
        inputImage=imread(inputImage);
    else
        error('The Color Type of Input Image is not acceptable. Acceptable color types are truecolor or grayscale.');
    end
end
% Check the size of window to see if it is an odd number.
if (mod(windowSize,2)==0)
    error('The window size must be an odd number.');
end
% Initialize the timer to calculate the time consumed.
tic;
% Find the size (columns and rows) of the image and assign the rows to
% variable nr, and columns to variable nc
[nr,nc] = size(inputImage);
% Create an image of size nr and nc, fill it with zeros and assign
% it to variable rankTransformedImage
rankTransformedImage = zeros(nr,nc);
% Find out how many rows and columns are to the left/right/up/down of the
% central pixel based on the window size
R= (windowSize-1)/2;
for (i=R+1:1:nr-R) % Go through all the rows in an image (minus R at the borders)
    for (j=R+1:1:nc-R) % Go through all the columns in an image (minus R at the borders)     
        rank = 0; % Initialize default rank to 0
        for (a=-R:1:R) % Within the square window, go through all the rows
            for (b=-R:1:R) % Within the square window, go through all the columns
                % If the intensity of the neighboring pixel is less than
                % that of the central pixel, then increase the rank
                if (inputImage(i+a,j+b) < inputImage(i,j))
                    rank=rank+1;
                end     
            end
        end
        % Assign the rank value to the pixel in imgTemp
        rankTransformedImage(i,j) = rank;
    end
end
% Stop the timer to calculate the time consumed.
timeTaken=toc;